const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({


	firstName:{
		type:String,
		required:[true, "First Name is Required!"]
	},

	lastName:{
		type:String,
		required:[true, "Family Name is Required!"]
	},
	email:{
		type:String,
		required:[true, "Email is Required!"]
	},
	password:{
		type:String,
		required:[true, "Password is Required!"]
	},
	isAdmin: {
		type: Boolean,
		default:true
	},
	mobileNumber:{
		type:String,
		required:[true, "Mobile number is Required!"]
	},
	enrollees:[{
			courseId:{
				type: String,
				required:[true,"courseId Id is required!"]
			},
			dateEnrolled:{
				type: Date,
				default: new Date()
			},
			status:{
				type: String,
				default: "Enrolled"
			}
	}
	]

})

module.exports = mongoose.model("User",courseSchema);